from peewee import *
from modelHero import *
from util import *
from const import *



def load():
	return ToList(Hero.select().dicts())



def loadById(idHero: int):
	ret = Hero.select().where(Hero.id == idHero)
	if (len(ret) > 0):
		return ret.dicts().get()
	else:
		return None


def create(hero: Hero):
	Hero.create(name=hero.name, idLol=hero.idLol, image=hero.image)


