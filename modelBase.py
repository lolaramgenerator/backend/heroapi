from peewee import *
from const import db
from playhouse.shortcuts import model_to_dict


class BaseModel(Model):
    class Meta:
        database = db

    def toDict(self, recursivo: bool):
        return model_to_dict(self, backrefs=recursivo)