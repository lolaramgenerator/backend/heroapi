from fastapi import FastAPI, HTTPException, Response, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi_utils.tasks import repeat_every
import time

from workerHeroImport import *
from caseHero import *
from util import *


app = FastAPI()


origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



@app.get("/Heroes/")
async def getHeroes():
    return caseLoadHeroes()


@app.get("/Heroes/{idHero}")
async def getHeroes(idHero: int):
    return ValidateReturn(caseLoadHeroById(idHero))


@app.on_event("startup")
@repeat_every(seconds=60 * 60 * 24)
def scheduleImportHores() -> None:
    HeroImport()